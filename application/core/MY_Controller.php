<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * A Controller base that provides lots of flexible basics for CodeIgniter applications,
 * including:
 *     - A basic template engine
 *     - More readily available 'flash messages'
 *     - autoloading of language and model files
 *     - flexible rendering methods making JSON simple.
 *     - Automatcially migrating.
 *
 * NOTE: This class assumes that a couple of other libraries are in use in your
 * application:
 *     - eldarion-ajax (https://github.com/eldarion/eldarion-ajax) for simple AJAX.
 *         Only used by the render_json method to return profiler info and
 *
 */
class MY_Controller extends CI_Controller
{

    /**
     * El tipo de caché a utilizar. Los valores predeterminados son
         * Establecer aquí para que puedan ser utilizados en todas partes, pero
     */
    protected $cache_type = 'dummy';
    protected $backup_cache = 'file';

    // Si TRUE, devolverá la vista de avisos
    // a través del método 'render_json' en el
    // 'fragmentos' array.
    protected $ajax_notices = true;

    // Si se establece, se cargará automáticamente.
    protected $language_file = null;

    // Si se establece, el modelo se cargará automáticamente.
    protected $model_file = null;

    private $use_view = '';
    private $use_layout = '';
    private $use_theme = 'frontend/';

    protected $external_scripts = array();
    protected $stylesheets = array();

    // Almacena variables de datos que se enviarán a la vista.
    protected $vars = array();

    // estados de  messages
    protected $message;

// Values to be obtained automatically from router
    protected $mModule = ''; // module name (empty = Frontend Website)
    protected $mCtrler = 'welcome'; // current controller
    protected $mAction = 'index'; // controller function being called
    protected $mMethod = 'GET'; // HTTP request method

    public function __construct()
    {
        parent::__construct();

        //--------------------------------------------------------------------
        // Cache Setup
        //--------------------------------------------------------------------

        // Make sure that caching is ALWAYS available throughout the app
        // though it defaults to 'dummy' which won't actually cache.
        $this->load->driver('cache', array('adapter' => $this->cache_type, 'backup' => $this->backup_cache));

        //--------------------------------------------------------------------
        // Configuracion del site
        //--------------------------------------------------------------------
        $site_config = $this->config->item('sitio_cfg');

        $this->set_var('menu', $site_config['menu']);

        if ($site_config['opciones'] == true) //&& is_array($this->vars['opciones']))
        {
            $this->load->model('Opciones_model');

            $opciones['item'] = new stdClass();

            foreach ($this->Opciones_model->get_all() as $setting) {
                $opciones['item']->{$setting->key} = $setting->value;
            }

            $this->set_var('opciones', $opciones);
            $this->set_var('page_title', $opciones['item']->title);
            $this->set_var('name_back', $opciones['item']->title);

        } elseif ($site_config['opciones'] == false) {

            $this->set_var('page_title', $site_config['page_title']);
            $this->set_var('name_back', $site_config['name']);

        }

        $this->external_scripts = $site_config['scripts'];
        $this->stylesheets = $site_config['stylesheets'];

        //--------------------------------------------------------------------
        // Language & Model Files
        //--------------------------------------------------------------------

        if (!is_null($this->language_file)) {
            $this->lang->load($this->language_file);
        }

        if (!is_null($this->model_file)) {
            $this->load->model($this->model_file);
        }

        //--------------------------------------------------------------------
        // Profiler
        //--------------------------------------------------------------------

        // The profiler is dealt with twice so that we can set
        // things up to work correctly in AJAX methods using $this->render_json
        // and it's cousins.
        if ($this->config->item('show_profiler') == true) {
            $this->output->enable_profiler(true);
        }

        //--------------------------------------------------------------------
        // Development Environment Setup
        //--------------------------------------------------------------------
        //

        if (ENVIRONMENT == 'development')
        {
	           
				if ($debug_config['view_data'])
					$this->output->append_output('<hr/>'.print_r($this->mViewData, TRUE));
		
        }

        //--------------------------------------------------------------------
        // Custom url breacamp
        //--------------------------------------------------------------------
        //
        //echo   $this->mModule = $this->router->module;
        $this->mCtrler = $this->router->class;
        $this->mAction = $this->router->method;
        $this->mMethod = $this->input->server('REQUEST_METHOD');

        $this->load->library(array('breadcrumbs'));
        $this->load->helper('MY_Helper');
    }

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // "Template" Functions
    //--------------------------------------------------------------------

    /**
     * A Very simple templating system designed not for power or flexibility
     * but to use the built in features of CodeIgniter's view system to easily
     * create fast templating capabilities.
     *
     * The view is assumed to be under the views folder, under a folder with the
     * name of the controller and a view matching the name of the method.
     *
     * The theme is simply a set of files located under the views/ui folder. By default
     * a view named index.php will be used. You can specify different layouts
     * with the scope method, 'layout()'.
     *
     *      $this->layout('two_left')->render();
     *
     * You can specify a non-default view name with the scope method 'view'.
     *
     *      $this->view('another_view')->render();
     *
     * Within the template the string '{view_content}' will be replaced with the
     * contents of the view file that we're rendering.
     *
     * @param  [type]  $layout      [description]
     * @param  boolean $return_data [description]
     * @return [type]               [description]
     */
    protected function render($data = array(), $rtn_html = false)
    {
        // Calc our view name based on current method/controller
        $view = !empty($this->use_view) ? $this->use_view : $this->mCtrler . '/' . $this->mAction;

        // Merge any saved vars into the data
        $data = array_merge($data, $this->vars);
        $data['current_uri'] = empty($this->mCtrler) ? uri_string() : str_replace($this->mCtrler . '/', '', uri_string());
        $data['ctrler'] = $this->mCtrler;

        // Make sure any scripts/stylesheets are available to the view
        $data['external_scripts'] = $this->external_scripts;
        $data['stylesheets'] = $this->stylesheets;

        // We'll make the view content available to the template.
        $data['view_content'] = $this->load->view($this->use_theme.$view, $data, true);

        // Build our notices from the theme's view file.
        $data['notice'] = $this->load->view($this->use_theme.'theme/notice', array('notice' => $this->message()), true);

        // Render our layout and we're done!
        $layout = !empty($this->use_layout) ? $this->use_layout : 'index';

        $view_html = $this->load->view($this->use_theme.'theme/' . $layout, $data, $rtn_html, true);

        // Reset our custom view attributes.
        $this->use_view = $this->use_layout = '';

        if ($rtn_html) {
            return $view_html;
        }

    }

    //--------------------------------------------------------------------

    /**
     * Sets a data variable to be sent to the view during the render() method.
     *
     * @param string $name
     * @param mixed $value
     */
    public function set_var($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $k => $v) {
                $this->vars[$k] = $v;
            }
        } else {
            $this->vars[$name] = $value;
        }
    }

    //--------------------------------------------------------------------

    /**
     * Specifies a custom view file to be used during the render() method.
     * Intended to be used as a chainable 'scope' method prioer to calling
     * the render method.
     *
     * Examples:
     *      $this->view('my_view')->render();
     *      $this->view('users/login')->render();
     *
     * @param  string $view The relative path/name of the view file to use.
     * @return MY_Controller instance
     */
    public function view($view)
    {
        $this->use_view = $view;

        return $this;
    }

    //--------------------------------------------------------------------

    /**
     * Specifies a custom layout file to be used during the render() method.
     * Intended to be used as a chainable 'scope' method prioer to calling
     * the render method.
     *
     * Examples:
     *      $this->layout('two_left')->render();
     *
     * @param  string $view The relative path/name of the view file to use.
     * @return MY_Controller instance
     */
    public function layout($view)
    {
        $this->use_layout = $view;

        return $this;
    }


    public function theme($view)
    {
        $this->use_theme = $view;

        return $this;
    }

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // Status Messages
    //--------------------------------------------------------------------

    /**
     * Sets a status message (for displaying small success/error messages).
     * This is used in place of the session->flashdata functions since you
     * don't always want to have to refresh the page to show the message.
     *
     * @param string $message The message to save.
     * @param string $type    The string to be included as the CSS class of the containing div.
     */
    public function set_message($message = '', $type = 'info')
    {
        if (!empty($message)) {
            if (isset($this->session)) {
                $this->session->set_flashdata('message', $type . '::' . $message);
            }

            $this->message = array(
                'type' => $type,
                'message' => $message,
            );
        }
    }

    //--------------------------------------------------------------------

    /**
     * Recupera el mensaje, para mostrar (si lo hay).
     *
     * @param  string $message [description]
     * @param  string $type    [description]
     * @return array
     */
    public function message($message = '', $type = 'info')
    {
        $return = array(
            'message' => $message,
            'type' => $type,
        );

        // Does session data exist?
        if (empty($message) && class_exists('CI_Session')) {
            $message = $this->session->flashdata('message');

            if (!empty($message)) {
                // saca las partes del mensaje
                // pero tb puede ser un array

                $temp_message = explode('::', $message);
                if (count($temp_message) > 1) {
                    $return['type'] = $temp_message[0];
                    $return['message'] = $temp_message[1];
                } else {
                    $return['message'] = $message;
                }
                unset($temp_message);
            }
        }

        // If message is empty, we need to check our own storage.
        if (empty($message)) {
            if (empty($this->message['message'])) {
                return '';
            }

            $return = $this->message;
        }

        // Clear our session data so we don't get extra messages on rare occassions.
        if (class_exists('CI_Session')) {
            $this->session->set_flashdata('message', '');
        }

        return $return;
    }

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // Other Rendering Methods
    //--------------------------------------------------------------------

    /**
     * Renders a string of aribritrary text. This is best used during an AJAX
     * call or web service request that are expecting something other then
     * proper HTML.
     *
     * @param  string $text The text to render.
     * @param  bool $typography If TRUE, will run the text through 'Auto_typography'
     *                          before outputting to the browser.
     *
     * @return [type]       [description]
     */
    public function render_text($text, $typography = false)
    {
        // Note that, for now anyway, we don't do any cleaning of the text
        // and leave that up to the client to take care of.

        // However, we can auto_typogrify the text if we're asked nicely.
        if ($typography === true) {
            $this->load->helper('typography');
            $text = auto_typography($text);
        }

        $this->output->enable_profiler(false)
            ->set_content_type('text/plain')
            ->set_output($text);
    }

    //--------------------------------------------------------------------

    /**
     * Converts the provided array or object to JSON, sets the proper MIME type,
     * and outputs the data.
     *
     * Do NOT do any further actions after calling this action.
     *
     * @param  mixed $json  The data to be converted to JSON.
     * @return [type]       [description]
     */
    public function render_json($json, $f = true, $code = 200)
    {
        if (is_resource($json)) {
            throw new RenderException('Resources can not be converted to JSON data.');
        }

        // If there is a fragments array and we've enabled profiling,
        // then we need to add the profile results to the fragments
        // array so it will be updated on the site, since we disable
        // all profiling below to keep the results clean.
        if (is_array($json) && $f) {
            if (!isset($json['fragments'])) {
                $json['fragments'] = array();
            }

            if ($this->config->item('show_profile')) {
                $this->load->library('profiler');
                $json['fragments']['#profiler'] = $this->profiler->run();
            }

            // Also, include our notices in the fragments array.
            if ($this->ajax_notices === true) {
                $json['fragments']['#notices'] = $this->load->view($this->use_theme.'theme/notice', array('notice' => $this->message()), true);
            }

            $this->output->enable_profiler(false)
                ->set_content_type('application/json')
                ->set_output(json_encode($json));

        } else {
            $this->output
                ->set_status_header($code)
                ->set_content_type('application/json')
                ->set_output(json_encode($json));

            // force output immediately and interrupt other scripts
            global $OUT;
            $OUT->_display();
            exit;
        }

    }

    //--------------------------------------------------------------------

    /**
     * Sends the supplied string to the browser with a MIME type of text/javascript.
     *
     * Do NOT do any further processing after this command or you may receive a
     * Headers already sent error.
     *
     * @param  mixed $js    The javascript to output.
     * @return [type]       [description]
     */
    public function render_js($js = null)
    {
        if (!is_string($js)) {
            throw new RenderException('No javascript passed to the render_js() method.');
        }

        $this->output->enable_profiler(false)
            ->set_content_type('application/x-javascript')
            ->set_output($js);
    }

    //--------------------------------------------------------------------

    /**
     * Breaks us out of any output buffering so that any content echo'd out
     * will echo out as it happens, instead of waiting for the end of all
     * content to echo out. This is especially handy for long running
     * scripts like might be involved in cron scripts.
     *
     * @return void
     */
    public function render_realtime()
    {
        if (ob_get_level() > 0) {
            end_end_flush();
        }
        ob_implicit_flush(true);
    }

    //--------------------------------------------------------------------

    /**
     * Integrates with the bootstrap-ajax javascript file to
     * redirect the user to a new url.
     *
     * If the URL is a relative URL, it will be converted to a full URL for this site
     * using site_url().
     *
     * @param  string $location [description]
     */
    public function ajax_redirect($location = '')
    {
        $location = empty($location) ? '/' : $location;

        if (strpos($location, '/') !== 0 || strpos($location, '://') !== false) {
            if (!function_exists('site_url')) {
                $this->load->helper('url');
            }

            $location = site_url($location);
        }

        $this->render_json(array('location' => $location));
    }

    //--------------------------------------------------------------------

    /**
     * Intenta obtener cualquier información de php: // input y devolverla
         * como datos JSON. Esto es útil cuando su javascript está enviando datos JSON
         * a la aplicación.
     *
     * @param  strign $format   The type of element to return, either 'object' or 'array'
     * @param  int   $depth     The number of levels deep to decode
     *
     * @return mixed    The formatted JSON data, or NULL.
     */
    public function get_json($format = 'object', $depth = 512)
    {
        $as_array = $format == 'array' ? true : false;

        return json_decode(file_get_contents('php://input'), $as_array, $depth);
    }

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // 'Asset' functions add css / js
    //--------------------------------------------------------------------

    /**
     * Agrega un archivo javascript externo al array 'external_scripts'.
     *
     * @param string $filename [description]
     */
    public function add_script($filename, $append = true, $position = 'foot')
    {

        $filename = is_string($filename) ? array($filename) : $filename;

        $position = ($position === 'head' || $position === 'foot') ? $position : 'foot';

        if ($append) {
            $this->external_scripts[$position] = array_merge($this->external_scripts[$position], $filename);
        } else {
            $this->external_scripts[$position] = array_merge($filename, $this->external_scripts[$position]);
        }

    }
    //--------------------------------------------------------------------

    /**
     * Agrega un archivo/s stylesheets (css)  al array 'stylesheets' .
     */
    public function add_style($filename, $append = true, $media = 'screen')
    {

        $files = is_string($filename) ? array($filename) : $filename;

        if ($append) {
            $this->stylesheets[$media] = array_merge($this->stylesheets[$media], $files);
        } else {
            $this->stylesheets[$media] = array_merge($files, $this->stylesheets[$media]);
        }

    }
    //--------------------------------------------------------------------

    /**
     * controlasi hay un usuario logeado
     * @param  [type] $redirect_url [description]
     * @return [type]               [description]
     */
    protected function verify_login($redirect_url = null)
    {
        if (!$this->ion_auth->logged_in()) {
            if ($redirect_url == null) {
                $redirect_url = 'auth/login';
            }

            redirect($redirect_url);
        }
    }
    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // funciones de seguridad extraidas de ion_auth controla los
    // formularios si son generados por el sistema
    //--------------------------------------------------------------------

    /**
     * [_get_csrf_nonce description]
     * @return [type] [description]
     */
    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return true;
        } else {
            return false;
        }
    }
/**
 * devuelve una cadena con las mismas caracteristicas que render
 * @param  [type] $data   [description]
 * @param  string $layout [description]
 * @return [type]         [description]
 */
    protected function render_str($data = null, $layout = 'simple')
    {
        // Calc our view name based on current method/controller
        $view = !empty($this->use_view) ? $this->use_view : $this->router->fetch_class() . '/' . $this->router->fetch_method();

        // Merge any saved vars into the data
        $data = array_merge($data, $this->vars);

        // Make sure any scripts/stylesheets are available to the view
        $data['external_scripts'] = $this->external_scripts;
        $data['stylesheets'] = $this->stylesheets;

        // We'll make the view content available to the template.
        $data['view_content'] = $this->load->view($view, $data, true);

        // Build our notices from the theme's view file.
        $data['notice'] = $this->load->view($this->use_theme.'theme/notice', array('notice' => $this->message()), true);

        // Render our layout and we're done!
        $layout = !empty($this->use_layout) ? $this->use_layout : 'index';

        //$view_html =$this->load->view($view, $data, true);
        $view_html = $this->layout($layout)->view($view)->render($data);

        // Reset our custom view attributes.
        $this->use_view = $this->use_layout = '';

        return $view_html; //This will return html on 3rd argument being true
    }
    //--------------------------------------------------------------------

}

//--------------------------------------------------------------------
// include base controllers para que los busque cuando llame desde el admin
require APPPATH."core/Authenticated_Controller.php";
