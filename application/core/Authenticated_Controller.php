<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*
AUTHENTICATED CONTROLLER

Simply makes sure that someone is logged in and ready to roll.
 */

class Authenticated_Controller extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->verify_login();
      
        //--------------------------------------------------------------------
        // Configuracion del site
        //--------------------------------------------------------------------
        $site_config = $this->config->item('panel_cfg');

        if ($site_config['opciones'] == true) //&& is_array($this->vars['opciones']))
        {
            $this->load->model('Opciones_model');

            $opciones['item'] = new stdClass();

            foreach ($this->Opciones_model->get_all() as $setting) {
                $opciones['item']->{$setting->key} = $setting->value;
            }

            $this->set_var('opciones', $opciones);
            $this->set_var('page_title', $opciones['item']->title);
            $this->set_var('name_back', $opciones['item']->title);

        } elseif ($site_config['opciones'] == false) {

            $this->set_var('page_title', $site_config['page_title']);
            $this->set_var('name_back', $site_config['name']);

        }

        $this->external_scripts = $site_config['scripts'];
        $this->stylesheets = $site_config['stylesheets'];

        $this->theme('backend/');    
        
        $this->breadcrumbs->push('Inicio', 'admin/welcome');
        
        $this->load->library(array('form_builder','datatables'));

        $this->set_var('menu', $site_config['menu']);


    }

    //--------------------------------------------------------------------

}
?>