
        <h2 style="margin-top:0px">Setting Option</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('setting_options/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('setting_options/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('setting_options'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Setting</th>
		<th>Name</th>
		<th>Descrip</th>
		<th>Value</th>
		<th>Autoload</th>
		<th>Action</th>
            </tr><?php
            foreach ($setting_options_data as $setting_options)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $setting_options->setting ?></td>
            <!-- <td><?php //echo $setting_options->id_setting ?></td> -->
			<td><?php echo $setting_options->name ?></td>
			<td><?php echo $setting_options->descrip ?></td>
			<td><?php echo $setting_options->value ?></td>
			<td><?php echo $setting_options->autoload ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('setting_options/read/'.$setting_options->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('setting_options/update/'.$setting_options->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('setting_options/delete/'.$setting_options->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    