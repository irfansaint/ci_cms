
        <h2 style="margin-top:0px">Options <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Setting <?php echo form_error('id_setting') ?></label>
            <input type="hidden" class="form-control" name="id_setting" id="id_setting" placeholder="Id Setting" value="<?php echo $id_setting; ?>" />
            <?php echo $combo[$id_setting]; //form_dropdown('setting', $combo, $id_setting); ?>
            </label>
        </div>
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Descrip <?php echo form_error('descrip') ?></label>
            <input type="text" class="form-control" name="descrip" id="descrip" placeholder="Descrip" value="<?php echo $descrip; ?>" />
        </div>
	    <div class="form-group">
            <label for="value">Value <?php echo form_error('value') ?></label>
            <textarea class="form-control" rows="3" name="value" id="value" placeholder="Value"><?php echo $value; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="tinyint">Autoload <?php echo form_error('autoload') ?></label>
            <input type="text" class="form-control" name="autoload" id="autoload" placeholder="Autoload" value="<?php echo $autoload; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('setting_options') ?>" class="btn btn-default">Cancel</a>
	</form>
    