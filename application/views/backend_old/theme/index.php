<?php $this->load->view('backend/theme/header'); ?>

<body class="" id="page-top">
<div class="preloade">
            <img src="<?php echo base_url(UPLOAD_IMG.$opciones['item']->logo)?>">
        </div>
  <div id="wrapper"> 
    <!-- MENU -->
    <?php $this->load->view('backend/theme/menu'); ?>

    <div id="page-wrapper">

      <div class="container-fluid">
                <!-- Breadcrumbs -->
                <?php echo $this->breadcrumbs->show();?>
                <!-- notice / messages -->
                <?php if (isset($notice)) : ?>
                  <?php echo $notice; ?>
                <?php else : ?>
                  <div id="notices"></div>
                <?php endif; ?>


                        <?= $view_content ?>

                    
      </div> 
              <!-- /.container-fluid-->

       <!-- /.wrapper-->
    </div> 

  </div> 
    <?php $this->load->view('backend/theme/footer'); ?>
