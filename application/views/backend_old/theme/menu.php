<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top fixed-top" role="navigation" id="mainNav">
 <div class="navbar-header"> 
   <a class="navbar-brand" href="<?php echo base_url('admin')?>" title="<?=$name_back?>">
   <img class="sislogo " src="<?php echo base_url(UPLOAD_IMG.$opciones['item']->logo)?>" title="<?=$name_back?>" alt="<?=$name_back?>">
   <!-- <img class="sislogo " src="<?php echo base_url('assets/images/logo.png')?>" alt="<?=$name_back?>"> -->
  </a>
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarResponsive">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>
<div class="collapse navbar-collapse " id="navbarResponsive">
<ul class="nav navbar top-nav">
<?php foreach ($menu as $parent => $parent_params): ?>

<?php 
$parent_active =null;
if (empty($parent_params['children'])): ?>

 <?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
 <li class='nav-item  <?php if ($active) echo 'active'; ?> ' title="<?php echo $parent_params['name']; ?>">
  <a class="nav-link" href='<?php echo site_url('admin/'.$parent_params['url']) ; ?>'>
   <i class='<?php echo $parent_params['icon']; ?>'></i>
   <span class="nav-link-text"><?php echo $parent_params['name']; ?></span>
 </a>
</li>

<?php else: ?>

<?php if (in_array($ctrler, $parent_params['children']) ) $parent_active ="active"; ?>

<?php //if($parent_active) ?>
  <li class="dropdown nav-item <?php echo $parent_active; ?>" >
    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" >
<i class='<?php echo $parent_params['icon']; ?>'></i>
<span class="nav-link-text"><?php echo $parent_params['name']; ?>
<i class="fa fa-fw fa-caret-down"></i> 
</span>
    </a>
    <ul class="dropdown-menu " id="<?php echo $parent_params['name']; ?>">
     <?php foreach ($parent_params['children'] as $name => $url): ?>
      <?php $child_active = ($ctrler==$url); ?>
      <li class="nav-item <?php if ($child_active) echo 'active'; ?>" >
       <a href='<?php echo site_url('admin/'.$url); ?>' ><i  class="fa fa-circle-o "></i> <?php echo $name; ?></a>
     </li>
   <?php endforeach; ?>
 </ul>
</li>

<?php endif; ?>

<?php endforeach; ?>
   <li class="nav-item">
    <a class="nav-link" data-toggle="modal" data-target="#exampleModal" href="#">
      <i class="fa fa-fw fa-sign-out"></i>Salir</a>
    </li>
  </ul>
  </div>
  <!-- <div class="collapse navbar-collapse " id="navbarResponsive">
    <ul class="nav navbar-nav side-nav navbar-sidenav">
      <?php foreach ($menu as $parent => $parent_params): ?>

        <?php if (empty($parent_params['children'])): ?>

         <?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
         <li class='nav-item  <?php if ($active) echo 'active'; ?> ' title="<?php echo $parent_params['name']; ?>">
          <a class="nav-link" href='<?php echo site_url($parent_params['url']) ; ?>'>
           <i class='<?php echo $parent_params['icon']; ?>'></i>
           <span class="nav-link-text"><?php echo $parent_params['name']; ?></span>
         </a>
       </li>

     <?php else: ?>

       <?php $parent_active = ($ctrler==$parent); ?>

       <?php if($parent_active){ ?>
       <li class="" data-toggle="tooltip" data-placement="right" title data-original-title="<?php echo $parent_params['name']; ?>">
        <a  class=" nav-link nav-link-collapse " data-toggle="collapse" href="#<?php echo $parent_params['name']; ?>" data-parent="#exampleAccordion" aria-expanded="true">
          <?php }else{?>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title data-original-title="<?php echo $parent_params['name']; ?>">
        <a href="javascript:;" data-toggle="collapse" data-target="#<?php echo $parent_params['name']; ?>">
                  <?php }?>
 <i class='<?php echo $parent_params['icon']; ?>'></i>
 <span class="nav-link-text"><?php echo $parent_params['name']; ?>
  <i class="fa fa-fw fa-caret-down"></i> 
 </span>
            </a>
            <ul class="collapse <?php echo ($parent_active)?' show':'';?>" id="<?php echo $parent_params['name']; ?>">
             <?php foreach ($parent_params['children'] as $name => $url): ?>
              <?php $child_active = ($ctrler.'/'.$current_uri==$url); ?>
              <li class="nav-item <?php if ($child_active) echo 'active'; ?>" >
               <a href='<?php echo site_url($url); ?>' ><i  class="fa fa-circle-o "></i> <?php echo $name; ?></a>
             </li>
           <?php endforeach; ?>
         </ul>
       </li>

     <?php endif; ?>

   <?php endforeach; ?>
   <li class="divider"></li>
   <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Salir del Sistema">
    <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
      <i class="fa fa-fw fa-sign-out"></i>  <span class="nav-link-text">Salir</span></a>
    </li>
    

      <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
          <i class="fa fa-fw fa-angle-left"><</i>
        </a>
      </li>
    </ul>
  
</div>
 -->
</nav>  
