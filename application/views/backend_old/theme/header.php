<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?=$opciones['item']->description?>">
  <meta name="author" content="<?=$opciones['item']->autor?>">
  <title><?=$page_title;?></title>
  
<link rel="apple-touch-icon" href="<?=base_url(UPLOAD_IMG.$opciones['item']->favicon)?>">

<link rel="icon" type="image/png"  href="<?=base_url(UPLOAD_IMG.$opciones['item']->favicon)?>">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?=base_url(UPLOAD_IMG.$opciones['item']->favicon)?>">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript">
        var JS_BASE_URL = '<?php echo site_url('admin/'); ?>';
      </script>
<?php

if (isset($stylesheets))
{
    foreach ($stylesheets as $media => $files)
    {
      foreach ($files as $style ){
        $url = strpos($style, 'http') ? $style : base_url('assets/'.$style);
        echo "<link rel='stylesheet' type='text/css' href='{$url}' media='{$media}'/>\n";
      }
    }
}

   if (isset($external_scripts['head']))
  {
                      foreach ($external_scripts['head'] as $script)
                      {
                      $url = strpos($script, "http")!==false ? $script : base_url('assets/'.$script);
                          echo "<script src='{$url}'></script>\n";
                      }
  }

    ?>
     </head>
