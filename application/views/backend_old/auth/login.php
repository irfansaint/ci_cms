

<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="panel panel-default ">
        <div class="panel-heading"><?php echo lang('login_heading');?></div>
        <div class="panel-body">
          <?php echo lang('login_subheading');?>
          <?php echo form_open("auth/login");?>

          <p>
            <?php echo lang('login_identity_label', 'identity');?>
            <?php echo form_input($identity);?>
          </p>

          <p>
            <?php echo lang('login_password_label', 'password');?>
            <?php echo form_input($password);?>
          </p>

          <p>
            <?php echo lang('login_remember_label', 'remember');?>
            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
          </p>


          <p>
            <?php echo form_submit('submit', lang('login_submit_btn'),array('class' =>'btn btn-primary btn-block' ));?></p>

          <?php echo form_close();?>

          <p><a href="<?php echo site_url('auth/forgot_password')?>"  class="d-block small"><?php echo lang('login_forgot_password');?></a></p>
        </div>
      </div>
    </div>

    <div class="col-md-6"><img src="<?php echo base_url('assets/images/logo.png')?>"  alt="Mi Control"></div>
  </div>
</div>
