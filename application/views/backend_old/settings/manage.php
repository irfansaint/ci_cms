
    <div class="panel-body">
        <div class="row">
            <form role="form" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <?php if (validation_errors()): ?>
                    <div class="error"><?php echo validation_errors() ?></div>
                <?php endif ?> 
                <div class="col-md-12">
                    <ul class="nav nav-tabs nav-tabs-justified">
                        <li class="active">
                            <a href="#setting" data-toggle="tab">
                                <i class="fa fa-cog"></i>
                                <span class="hidden-xs">Configuración</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#contactinfo" data-toggle="tab">
                                <i class="fa fa-shopping-bag"></i>
                                <span class="hidden-xs">Informacion Comercial</span>
                            </a>
                        </li>
                        <li>
                            <a href="#soporteinfo" data-toggle="tab">
                                <i class="fa fa-life-ring "></i>
                                <span class="hidden-xs">Soporte</span>
                            </a>
                        </li>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="setting">
                        <hr>   
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nombre </label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[title]', set_value('setting[title]', $item->title), 'class="bg-focus form-control" data-required="true" id="title"') ?>
                                </div>
                            </div>
                            
                            <hr>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Logo</label>
                                <div class="compose-message-editor col-sm-8">
                                    <input type="file" class="form-control" name="logo">
                                </div>
                                <div class="col-sm-2">
                                    <?php if ($item->logo): ?>
                                        <img src="<?php echo base_url().UPLOAD_IMG.$item->logo ?>" class="img-inline userpic-32" width="40"/>
                                    <?php endif ?>
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <label class="col-sm-2 control-label">favicon</label>
                                <div class="compose-message-editor col-sm-8">
                                    <input type="file" class="form-control" name="favicon">
                                </div>
                                <div class="col-sm-2">
                                    <?php if ($item->favicon): ?>
                                        <img src="<?php echo base_url() .UPLOAD_IMG.$item->favicon ?>" class="img-inline userpic-32" width="40"/>
                                    <?php endif ?>
                                </div>

                            </div>
                            <hr>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo lang('settings_time_zone') ?></label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[default_timezone]', set_value('setting[default_timezone]', $item->default_timezone), 'class="bg-focus form-control" data-required="true" id="default_timezone"') ?>
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Descripción</label>
                                <div class="compose-message-editor col-sm-10">
                                    <textarea style="height: 150px;" class="form-control" name="setting[description]"><?php echo set_value('setting[description]', $item->description) ?></textarea>
                                </div>
                            </div>
                            <hr>
                        </div>




                        <div class="tab-pane" id="contactinfo">
                        <hr>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Razón Social</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[razon_social]', set_value('setting[razon_social]', $item->razon_social), 'class="bg-focus form-control" data-required="true" id="razon_social"') ?>
                                </div>
                            </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">CUIT</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[cuit]', set_value('setting[cuit]', $item->cuit), 'class="bg-focus form-control" data-required="true" id="cuit"') ?>
                                </div>
                            </div>
                            <div class="form-group">
                            <label class="col-sm-2 control-label">Dirección</label>
                                <div class="col-sm-10">
                                <?php echo form_input('setting[direction]', set_value('setting[direction]', $item->direction), 'class="bg-focus form-control" data-required="true" id="direction"') ?>
                                    
                                </div>
                                </div>    
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Telefono</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[phone]', set_value('setting[phone]', $item->phone), 'class="bg-focus form-control" data-required="true" id="phone"') ?>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">E-mail de Contacto</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[contact_form_email]', set_value('setting[contact_form_email]', $item->contact_form_email), 'class="bg-focus form-control" data-required="true" id="contact_form_email"') ?>
                                </div>
                            </div>
                            <hr>

                           

                            
                            <hr>
                        </div>
                        <div class="tab-pane" id="soporteinfo">
                        <hr>
                        
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Soporte</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[support]', set_value('setting[support]', $item->support), 'class="bg-focus form-control" data-required="true" id="support"') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Telefono</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[support_phone]', set_value('setting[support_phone]', $item->support_phone), 'class="bg-focus form-control" data-required="true" id="support_phone"') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">E-mail</label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[support_mail]', set_value('setting[support_mail]', $item->support_mail), 'class="bg-focus form-control" data-required="true" id="support_mail"') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo('Copyright') ?></label>
                                <div class="col-sm-10">
                                    <?php echo form_input('setting[copyright]', set_value('setting[copyright]', $item->copyright), 'class="bg-focus form-control" data-required="true" id="copyright"') ?>
                                </div>
                                </div>
                                <hr>
                        </div>
           
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-secondary " name="submit" value="<?php echo('submit') ?>">
                                <a href="<?php echo site_url('welcome'); ?>" class="btn btn-danger"><?php echo('cancel') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
