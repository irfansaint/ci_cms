
        <h2 style="margin-top:0px">Blog <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Titulo <?php echo form_error('titulo') ?></label>
            <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Titulo" value="<?php echo $titulo; ?>" />
        </div>
	    <div class="form-group">
            <label for="bajada">Bajada <?php echo form_error('bajada') ?></label>
            <textarea class="form-control" rows="3" name="bajada" id="bajada" placeholder="Bajada"><?php echo $bajada; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="longtext">Contenido <?php echo form_error('contenido') ?></label>
            <textarea class="form-control" rows="3" name="contenido" id="contenido" placeholder="contenido"><?php echo $contenido; ?></textarea>

        </div>
        <div class="form-group">
            <label for="categoria">categoria <?php echo form_error('categoria') ?></label>
<?php
foreach ($categoria as $key => $value) {
    echo $value;?>

       <input type="checkbox" class="" name="categoria" id="categoria" placeholder="categoria" value="<?php echo $key ?>" />
   <?php
}
?>

        </div>
	    <div class="form-group">
            <label for="tinyint">Activo <?php echo form_error('activo') ?></label>
            <input type="text" class="form-control" name="activo" id="activo" placeholder="Activo" value="<?php echo $activo; ?>" />
        </div>

	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
	    <a href="<?php echo site_url('admin/blog') ?>" class="btn btn-default">Cancel</a>
	</form>



    <script src='<?php echo base_url(); ?>assets/js/ckeditor.js'></script>

    <script src='<?php echo base_url(); ?>assets/js/plugins/embed/plugin.js' type="module"></script>

    <script src='<?php echo base_url(); ?>assets/ckfinder/ckfinder.js'></script>


<script>
//import MediaEmbed from '<?php echo base_url(); ?>assets/js/plugins/embed/plugin.js';
	ClassicEditor
		.create( document.querySelector( '#contenido' ), {
               ckfinder: {
               // Upload the images to the server using the CKFinder QuickUpload command.
                uploadUrl: '<?php echo base_url(); ?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
            // Define the CKFinder configuration (if necessary).
                options: {
                    resourceType: 'Images'
                }
            },
           // plugins: ['MediaEmbed'],
            toolbar: [ 'ckfinder', 'heading', '|',
             'bold', 'italic', 'link', 'underline', 'strikethrough', 'code','subscript', 'superscript','|',
            'bulletedList', 'numberedList','|',
            'blockQuote','imageUpload','mediaEmbed']
		} )
		.then( editor => {
			window.CKEDITOR = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>