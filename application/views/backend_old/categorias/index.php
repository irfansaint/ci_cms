
<div class="">

<?php echo @Form::bs3_btn('<i class="fa fa-fw fa-file" title="Agregar Categoria"></i> Agregar Categoria',
        ["type"=>"button",
        "id"=>"box-modal","class"=>"btn btn-primary" ]); ?>
    <div id="fom-categoria">   </div>
    <hr/>

  <table id="myDataTable" class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Categoria Padre</th>
        <th>Accion </th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>


</div>
<script type="text/javascript">

$('#box-modal').on('click', function() {

$.get(JS_BASE_URL +"categorias/showform",function(data){
  $('#fom-categoria').html(data);
});
      bootbox.dialog({
          message: $('#fom-categoria'),
               })
               .on('shown.bs.modal', function() {
                $('#fom-categoria')
                  .show()   ; /* Reset form */
          })
          .on('hide.bs.modal', function(e) {
               $('#fom-categoria').hide().appendTo('body');
          }).modal('show');
                          
        });

$('#myDataTable').dataTable( {
      processing: true,
      serverSide: true,

      ajax: {
          "url": JS_BASE_URL +"categorias/json",
          "type": "POST"
      },
      "language": {
        "url": JS_BASE_URL +"../assets/vendor/datatables/Spanish.json"
            },
      columns: [
          {title : "Nombre", data: "name" },
          {title : "Categoria Padre", data : "parent_name" } ,
          {title : "Accion",  data:"id",
            "mRender": function ( data, type, full ) {
              return '<a href="'+JS_BASE_URL +'categorias/editform/'+full.id+'">Editar</a>';
            }
          }
      ]
  });
</script>
