<link href='<?php echo base_url();?>assets/css/fullcalendar.min.css' rel='stylesheet' />

<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
<!-- Custom css  -->
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet" />

<script src='<?php echo base_url();?>assets/js/moment.min.js'></script>
<script src="<?php echo base_url();?>assets/js/fullcalendar.js"></script>
<script src='<?php echo base_url();?>assets/js/locale-all.js'></script>
<script src='<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js'></script>



<!-- Notification -->
<div class="alert"></div>
<div class="row clearfix">
	<div class="col-md-12 column">
		<div id='calendar'></div>
	</div>
</div>

<div class="modal fade" id="addCalendar">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title"></h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				
			</div>
			<div class="modal-body">
				<div class="error"></div>
				<form class="form-horizontal" id="crud-form">
					<input type="hidden" id="start">
					<input type="hidden" id="end">
					<div class="form-group">
						<label class="control-label" for="paciente">Paciente</label>
						<div class="">
							<input id="paciente" name="paciente" type="text" placeholder="apellido nombre o DNI" class="form-control " />
                            <input type="hidden" name="id_paciente" id="id_paciente">
						</div>
					</div>
                    <div class="form-group">
						<label class=" control-label" for="title">Titulo</label>
						<div class="">
							<input id="title" name="title" type="text" class="form-control input-md" />
						</div>
					</div>
					<div class="form-group">
						<label class=" control-label" for="description">Descripcion</label>
						<div class="">
							<textarea class="form-control" id="description" name="description"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class=" control-label" for="color">Color</label>
						<div class="">
							<input id="color" name="color" type="text" class="form-control input-md" readonly="readonly" />
							<span class="help-block">Seleccione un color</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-secondary  float-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<script src='<?php echo base_url();?>assets/js/calendar.ctrl.js'></script>
