  <table id="table-buscador" class="table table-striped table-hover" style="width:100%">
    <thead>
      <tr>
      <th width="80px">No</th>
		    <th>Apellido</th>
		    <th>Nombre</th>
		    <th>Fecha Nac</th>
            <th>Domicilio</th>
		    <th>telefono</th>
        <th >Accion</th>
            </tr>
    </thead>
    <tbody></tbody>
  </table>

<script type="text/javascript">
var table=$('#table-buscador').dataTable( {
      processing: true,
      serverSide: true,
      keys: {
            blurable: false
        },
      ajax: {
          "url": JS_BASE_URL +"buscador/json",
          "type": "POST"
      },
      "language": {
                "url": JS_BASE_URL +"assets/vendor/datatables/Spanish.json"
            },
            columns: [
                        {
                            "data": "id",
                            "orderable": false,
                            "searchable":false,
                            
                        },{"data": "apellido"},
                        {"data": "nombre"},
                        {"data": "fecha_nac"},
                        {"data": "domicilio"},
                        {"data": "telefono"},
                        {
                            "data" : "agregar",
                            "orderable": false,
                            "searchable":false,
                            "className" : "text-center"
                        }
                       
                    ],
  });
  setTimeout( function() { 
    $('div.dataTables_filter input[type=search]').focus();
}, 500 );
 
</script>
