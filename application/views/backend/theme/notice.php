<div id="notices" class="row">


        <?php if (isset($notice) && is_array($notice)) : ?>
        <div class="col-md-12 alert alert-<?php echo $notice['type']; ?> alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $notice['message']; ?>
        </div>
        <?php endif; ?>

</div>
