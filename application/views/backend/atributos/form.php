

<?php echo $form->messages(); ?>

<div class="row">



      <div class="col-md-6">
				<?php echo $form->open(); ?>

            <div class="box box-info ">
              			<div class="box-header">
              				<h3 class="box-title">Atributo Nuevo</h3>
              			</div>
              			<div class="box-body">
											<?php echo $form->field_hidden('id_rubro',$id_rubro); ?>
                      <?php echo $form->bs3_text('Label <span class="text-danger">(*)</span>', 'atributo_label','',array('id'=>'atributo_label','required'=>'required')); ?>
              				<?php echo $form->bs3_text('Nombre del Atributo <span class="text-danger">(*)</span>', 'atributo_name','',array('id'=>'atributo_name','required'=>'required')); ?>
                      <?php echo $form->bs3_dropdown('Tipo de Presentación <span class="text-danger">(*)</span>', 
                                                    'atributo_presentacion',
                                                    array( 'text'=>'Texto',
                                                      'date'=>'Fecha',
                                                      'number'=>'Numero',
                                                      'bool'=>'Si/No',
                                                      'combo'=>'Combo',
                                                      'multi'=>'Multiple Seleccion',
                                                      
                                                      ),
                                                    array('id'=>'atributo_presentacion')); ?>
											<?php echo $form->bs3_submit('Agregar <i class="fa fa-plus-square" ></i>'); ?>

            </div>
        </div>
				<?php echo $form->close(); ?>
			</div>
				<div class="col-md-6">
					<div class="box box-info">
	              			<div class="box-header">
	              				<h3 class="box-title">Listados de Atributos</h3>
	              			</div>
											<div class="box-body no-padding">
												<table class="table table-hover table-striped" id="listado-attrib">
													<thead>
																	 <tr>
																	 <th>Label</th><th>nombre</th><th>Vista</th><th>-</th>
																 </tr>
												 </thead>
												   <tbody>
														 <?php foreach ($datos as $key => $value): ?>
															 <tr id="nl-<?php echo $value['id']?>">
                               <td>
                               <u data-pk="<?php echo $value['id']?>">
                               <?php echo $value['attrib_label']?>
																	</u>
																	</td>
														 		<td>
                                 <a href="#" data-pk="<?php echo $value['id']?>">
																	<?php echo $value['attrib_name']?>
																	</a>
    														</td>
																<td><?php echo $value['attrib_value']?></td>
																<td>
																	<span class="btn btn-default btn-flat" onclick="javascript:agregar_valores(<?php echo $value['id']?>)"><i class="fa  fa-plus" ></i></span>

																	 <?php if (!$value['valores']) :?>
|
																		 <span class="btn btn-default btn-flat" onclick="javascript:eliminar_atributo(<?php echo $value['id']?>)"><i class="fa  fa-trash" ></i></span>
																	 <?php endif; ?>
																</td>
														 </tr>
														 <?php endforeach; ?>
										      </tbody>
										   </table>


	              			</div>
	            </div>
	        </div>


<style media="screen">
	#atributo_valor{
		text-transform: capitalize;
	}
</style>
<div class="col-md-12">
			<div class="box" id="carga_valor_attrib">
							<div class="box-header">
								<h3 class="box-title">Valores del Atributo <b><span id="rt_name_attrib" class="">{Nombre Atributo}</span></b> </h3>
							</div>
							<div class="box-body">
								<div class="col-md-4">
									<?php echo $formitem->open(); ?>
										  <?php echo $form->field_hidden('id_atrrib',NULL); ?>
                      
											<?php echo $formitem->bs3_text('Item del Atributo <span class="text-danger">(*)</span>', 'atributo_valor','',array('id'=>'atributo_valor','required'=>'required')); ?>
											<?php echo $formitem->bs3_submit('Agregar [+]'); ?>
								<?php echo $formitem->close(); ?>

										</div>
										<div class="col-md-6">

											<table id="table-item-valor" class="table table-hover table-striped">
												<thead>
													<tr>

													<th>Nombre Item</th><th>Valor item</th><th>-</th>
												</tr>
											</thead>
											<tbody >

											</tbody>
										</table>
										</div>


							</div>
			</div>
	</div>


</div>
<script type="text/javascript">


  $(document).ready(function() {
    $("#carga_valor_attrib").hide();

    $('#listado-attrib a').editable({
      type: 'text',
      name: 'name_attrib',
      url: JS_BASE_URL +"/atributos/modNameAttrib",
      title: 'Nombre Atributo'
    });

    $('#listado-attrib u').editable({
      type: 'text',
      name: 'name_attrib',
      url: JS_BASE_URL +"/atributos/modLabelAttrib",
      title: 'Label Atributo'
    });



    $("#form-attrib").bootstrapValidator({

    }).on("success.form.bv", function(e) {
      // Prevent form submission
      e.preventDefault();

      // Get the form instance
      var $form = $(e.target);

      // Get the BootstrapValidator instance
      var bv = $("#form-attrib").data("bootstrapValidator");


      // Use Ajax to submit form data
      $.post($("#form-attrib").attr('action'), $('#form-attrib').serialize(), function(result) {
        if(result.id>0){
          $('#atributo_name').val('');
          //limpio la tabla de item
          $('#table-item-valor > tbody').html('');

          $('#listado-attrib').append('<tr id="nl-'+result.id+'">'+
          '<td>'+result.attrib_label+'</td>'+  
          '<td>'+result.attrib_name+'</td>'+
            '<td>'+result.attrib_value+'</td>'+
            '<td><span class="btn btn-default btn-flat" onclick="javascript:agregar_valores('+result.id+')"><i class="fa  fa-plus" ></i></span> | '+
              '<span class="btn btn-default btn-flat" onclick="javascript:eliminar_atributo('+result.id+')"><i class="fa  fa-trash" ></i></span>'+
              '</td></tr>');
              $("#carga_valor_attrib").hide();
              $("#form-attrib").data('bootstrapValidator').resetForm();
            }

          }, 'json');
        });

        $("#form-item-attrib").bootstrapValidator({

        }).on("success.form.bv", function(e) {
          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $("#form-item-attrib").data("bootstrapValidator");
          $("#table-item-valor").show();

          // Use Ajax to submit form data
          $.post($("#form-item-attrib").attr('action'), $('#form-item-attrib').serialize(), function(result) {
            if(result.id>0){
              $('#atributo_valor').val('');
              $('#table-item-valor').append('<tr id="nl-item-'+result.id+'">'+
                '<td>'+result.name+'</td>'+
                '<td>'+result.value+'</td>'+
                '<td><span class="btn btn-default btn-flat" onclick="javascript:edit_item('+result.id+')"><i class="fa  fa-edit" ></i></span> | '+
                  '<span class="btn btn-default btn-flat" onclick="javascript:eliminar_item('+result.id+')"><i class="fa  fa-trash" ></i></span>'+
                  '</td></tr>');

                  $("#form-item-attrib").data("bootstrapValidator").resetForm();
                }

              }, 'json');
            });


          });


          function editar_atributo(d){
            $("nl-"+d).text("").html("<input type='text' name='' value=''><a class='enlace guardar' href='#'>Guardar</a><a class='enlace cancelar' href='#'>Cancelar</a>");

          }

          function eliminar_atributo(d){
            // Use Ajax to submit form data
            $.post(JS_BASE_URL +"/atributos/delAttrib",{data : d}, function(result) {
              if(result==1){
                $("#nl-"+d).remove();
              }else if (result=="ko") {
                console.log('no borrar');
              }
              $("#form-item-attrib").data("bootstrapValidator").resetForm();
            }, 'json');
          }

          function agregar_valores(d){
            // Use Ajax to submit form data
            $.post(JS_BASE_URL +"/atributos/getValoresAttrib",{idattrib : d}, function(result) {
              $("#id_atrrib").val('');
              if (result!="ko") {
                $("#form-item-attrib").data("bootstrapValidator").resetForm();
                $("#table-item-valor").hide();
                $("#carga_valor_attrib").show();
                $('#table-item-valor > tbody').html('');
                $("#rt_name_attrib").text(result.attrib_name);
                $("#id_atrrib").val(result.id);

                if(result.valores.length>0){
                  $('#table-item-valor > tbody').html('');
                  $("#table-item-valor").show();
                  $.each(result.valores, function (index, item) {
                    var eachrow = "<tr id='nl-item-"+ item['id'] +"'>"
                      + "<td>"
                        + "<a href='#' data-pk='"+ item['id']+"'>"
                          + item['name']
                          +"</a>"
                          + "</td>"
                          + "<td>"
                          + "<u  data-pk='"+ item['id']+"'>"
                          + item['value'] 
                          +"</u>" 
                          +"<td><span class='btn btn-default btn-flat' onclick='javascript:edit_item("+ item['id'] +")'><i class='fa  fa-edit' ></i></span> | "
                            +"<span class='btn btn-default btn-flat' onclick='javascript:eliminar_item("+ item['id'] +")'><i class='fa  fa-trash' ></i></span>"
                            + "</td>"
                            + "</tr>";
                            $('#table-item-valor > tbody').append(eachrow);
                          });
                              ///Edita los atributo label
                          $('#table-item-valor a').editable({
                            type: 'text',
                            name: 'name_attrib',
                            url: JS_BASE_URL +"/atributos/modNameItemAttrib",
                            title: 'Nombre del Item'
                          });
                          //edita los atributos Valor
                          $('#table-item-valor u').editable({
                            type: 'text',
                            name: 'name_attrib',
                            url: JS_BASE_URL +"/atributos/modValueItemAttrib",
                            title: 'Nombre del Item'
                          });

                }

                        console.log(result.id);
                      }

                    }, 'json');

                  }
                  function eliminar_item(d){
                    // Use Ajax to submit form data
                    $.post(JS_BASE_URL +"/atributos/delItemAttrib",{data : d}, function(result) {
                      if(result==1){
                        $("#nl-item-"+d).remove();
                      }else if (result=="ko") {
                        console.log('no borrar');
                      }
                      $("#form-item-attrib").data("bootstrapValidator").resetForm();
                    }, 'json');
                  }



                </script>
