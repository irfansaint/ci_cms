
<?php echo $form->messages(); ?>


<?php echo $form->open(); ?>
		<div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Rubros</h3>
      </div>
      <div class="box-body">

        <?php $combo[0]='- Sin Rubro padre -';
        echo $form->bs3_dropdown('Rubro padre','parent', $combo,$id_parent,array( "placeholder"=>"Agregar Rubro Padre")); ?>
  <?php echo $form->field_hidden('id',$campos['id']); ?>
     
              			<div class="box-body">
	<?php echo $form->bs3_text('Nombre <span class="text-danger">(*)</span>', 'name',$campos['name'],array('required'=>'required')); ?>
   <?php echo $form->bs3_textarea('Descripción', 'description',$campos['description'],array('maxlength'=>"255",'required'=>'required')); ?>


              			</div>
            </div>
       
  

<div class="box-footer">
  <?php echo $form->bs3_submit('Guardar'); ?>

</div>
<?php echo $form->close(); ?>
</div>


<script type="text/javascript">


$(document).ready(function() {
    $('#form-rubros').bootstrapValidator({

    });
});
</script>
