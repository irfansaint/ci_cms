
<a href="<?=base_url('rubros/showform')?>" class="btn btn-primary">Agregar Rubro</a>
<hr/>
  <table id="myDataTable" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Rubro Padre</th>
        <th>Atributos </th>
        <th>Accion </th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>
<script type="text/javascript">
$('#myDataTable').dataTable( {
      processing: true,
      serverSide: true,

      ajax: {
          "url": JS_BASE_URL +"rubros/datatable",
          "type": "POST"
      },
      "language": {
                "url": ""+JS_BASE_URL+"assets/vendor/datatables/Spanish.json"
            },
      columns: [
          {title : "Nombre", data: "name" },
          {title : "Rubro Padre", data : "parent_name" } ,
          {title : "Atributos",  data: "id_parent",
          "mRender": function ( data, type, full ) {
            if(full.id_parent==0)
              return ' - ';
            return '<a href="'+JS_BASE_URL +'atributos/agregar/'+full.id+'">Agregar</a>';
          }
        },
          {title : "Accion",  data:"id",
            "mRender": function ( data, type, full ) {
              return '<a href="'+JS_BASE_URL +'rubros/editform/'+full.id+'">Editar</a>';
            }
          }
      ]
  });
</script>
