<div id="contact" class="section-padding">
      <div class="container">
            <div class="row">
                  <div class="col-12">
                        <div class="section-header text-center wow fadeInDown animated" data-wow-delay="0.3s"
                              style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                              <h2 class="section-title">Registrarse</h2>
                              <p><?php echo lang('create_user_subheading');?></p>
                        </div>
                  </div>
            </div>
            <div class="row contact-form-area wow fadeInUp animated" data-wow-delay="0.4s"
                  style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                  <div class="col-md-6 col-lg-4 col-sm-12">
                        <div class="contact-right-area wow fadeIn animated" style="visibility: visible;">
                              <h2>Get In Touch</h2>
                              <div class="contact-right">
                              </div>
                        </div>
                  </div>


                  <div class="col-md-6 col-lg-8 col-sm-12">
                        <div class="contact-block">
                              <?php echo form_open("usuario/registrar");?>

                              <p>
                                    <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                                    <?php echo form_input($first_name,['class'=>'form-control']);?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                                    <?php echo form_input($last_name);?>
                              </p>

                              <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

                              <p>
                                    <?php echo lang('create_user_email_label', 'email');?> <br />
                                    <?php echo form_input($email);?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_phone_label', 'phone');?> <br />
                                    <?php echo form_input($phone);?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_password_label', 'password');?> <br />
                                    <?php echo form_input($password,'',['class'=>'form-control']);?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
                                    <?php echo form_input($password_confirm,'',['class'=>'form-control']);?>
                              </p>


                              <p><?php echo form_submit('submit', lang('create_user_submit_btn'),["class"=>"btn btn-common blush disabled"]);?>
                              </p>

                              <?php echo form_close();?>
                        </div>
                  </div>
            </div>
      </div>
</div>