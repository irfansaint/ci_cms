
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="panel panel-default ">
      
    <div class="card-header"><?php echo lang('forgot_password_heading');?></div>
    <div class="card-body">
      <div class="text-center mt-4 mb-5">
  <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("admin/auth/forgot_password");?>

      <div class="form-group">
      	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php echo form_input($identity);?>
      </div>

      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

<?php echo form_close();?>
</div>
</div>
</div>
