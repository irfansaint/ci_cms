<?php $this->load->view('backend/theme/header'); ?>
<body class="bg-light" id="page-top">

  <div class="container-fluid">
   
<?php if (isset($notice)) : ?>
    <?php echo $notice; ?>
<?php else : ?>
    <div id="notices"></div>
<?php endif; ?>

</div>
            <?= $view_content ?>


<?php $this->load->view('backend/theme/footer'); ?>
