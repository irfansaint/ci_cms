
  <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
          aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          <span class="icon-menu"></span>
          <span class="icon-menu"></span>
          <span class="icon-menu"></span>
        </button>
        <a href="<?php echo site_url() ; ?>" class="navbar-brand"><img
            src="<?php echo base_url(UPLOAD_IMG.$opciones['item']->logo)?>" alt="<?=$name_back?>"></a>
      </div>
      <div class="collapse navbar-collapse" id="main-navbar">
        <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
          <?php foreach ($menu as $parent => $parent_params): ?>

          <?php 
$parent_active =null;
if (empty($parent_params['children'])): ?>

          <?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
          <li class="nav-item  <?php if ($active) echo 'active'; ?>" title="<?php echo $parent_params['name']; ?>">
            <a class="nav-link" href="<?php echo site_url($parent_params['url']) ; ?>">
              <?php echo $parent_params['name']; ?>
            </a>
          </li>
          <?php else: ?>
          <?php if (in_array($ctrler, $parent_params['children']) ) $parent_active ="active"; ?>
          <li class="dropdown nav-item <?php echo $parent_active; ?>">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <span class="nav-link-text"><?php echo $parent_params['name']; ?>
                <i class="fa fa-fw fa-caret-down"></i>
              </span>
            </a>
            <ul class="dropdown-menu " id="<?php echo $parent_params['name']; ?>">
              <?php foreach ($parent_params['children'] as $name => $url): ?>
              <?php $child_active = ($ctrler==$url); ?>
              <li class="nav-item <?php if ($child_active) echo 'active'; ?>">
                <a href='<?php echo site_url($url); ?>'> <?php echo $name; ?></a>
              </li>
              <?php endforeach; ?>
            </ul>
          </li>
          <?php endif; ?>
          <?php endforeach; ?>
        </ul>
        <div class="btn-register float-right">
          <a class="btn btn-border" href="../html/page-login.html">Registrar</a>
        </div>
        <div class="btn-sing float-right">
          <a class="btn btn-border" href="../html/page-login.html">Ingresar</a>
        </div>
      </div>
    </div>

    <ul class="mobile-menu navbar-nav">

      <?php foreach ($menu as $parent => $parent_params): ?>

      <?php 
$parent_active =null;
if (empty($parent_params['children'])): ?>

      <?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
      <li title="<?php echo $parent_params['name']; ?>">
        <a class="page-scroll" href='<?php echo site_url($parent_params['url']) ; ?>'>
          <?php echo $parent_params['name']; ?>
        </a>
      </li>

      <?php else: ?>

      <?php if (in_array($ctrler, $parent_params['children']) ) $parent_active ="active"; ?>

      <?php //if($parent_active) ?>
      <li class="dropdown nav-item <?php echo $parent_active; ?>">
        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
          <i class='<?php echo $parent_params['icon']; ?>'></i>
          <span class="nav-link-text"><?php echo $parent_params['name']; ?>
            <i class="fa fa-fw fa-caret-down"></i>
          </span>
        </a>
        <ul class="dropdown-menu " id="<?php echo $parent_params['name']; ?>">
          <?php foreach ($parent_params['children'] as $name => $url): ?>
          <?php $child_active = ($ctrler==$url); ?>
          <li class="nav-item <?php if ($child_active) echo 'active'; ?>">
            <a href='<?php echo site_url($url); ?>'><i class="fa fa-circle-o "></i> <?php echo $name; ?></a>
          </li>
          <?php endforeach; ?>
        </ul>
      </li>

      <?php endif; ?>

      <?php endforeach; ?>

    </ul>

  </nav>
