<?php if (isset($notice) && is_array($notice)) : ?>
<section id="notices" class="section-padding alert " role="alert">
  
        <div class="col-md-12  alert-<?php echo $notice['type']; ?> alert-dismissible" >
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $notice['message']; ?>
        </div>
       

</section>
 <?php endif; ?>
