<?php $this->load->view('frontend/theme/header'); ?>
<body class=" bg-dark" id="page-top">

  <div class="container">
   
<?php if (isset($notice)) : ?>
    <?php echo $notice; ?>
<?php else : ?>
    <div id="notices"></div>
<?php endif; ?>

</div>
            <?= $view_content ?>


<?php $this->load->view('frontend/theme/footer'); ?>
