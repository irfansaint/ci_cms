
            <?php $menu = get_main_menu(1);
                    $style='default'; ?>
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
          aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          <span class="icon-menu"></span>
          <span class="icon-menu"></span>
          <span class="icon-menu"></span>
        </button>
        <a href="<?php echo site_url() ; ?>" class="navbar-brand"><img
            src="<?php echo base_url(UPLOAD_IMG.$opciones['item']->logo)?>" alt="<?=$name_back?>"></a>
      </div>
      <div class="collapse navbar-collapse" id="main-navbar">
        <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
        <?php for ($i = 0; $i < count($menu->main_menu, true); $i++) { ?>
                            <?php if (count($menu->main_menu[$i]->parent_menu, true) == 0): ?>
                                <li class="nav-item"><a
                                            href="<?php echo base_url() . $menu->main_menu[$i]->url ?>" class="nav-link">
                                        <?php echo
                                        $menu->main_menu[$i]->title ?></a></li>
                            <?php else: ?>
                                <li class="dropdown nav-item">
                                    <a href="#"
                                    class="dropdown-toggle nav-link" data-toggle="dropdown">
                                        <span class="nav-link-text"><?php
                                        echo $menu->main_menu[$i]->title ?>   <i class="fa fa-fw fa-caret-down"></i>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu" id="<?php
                                        echo $menu->main_menu[$i]->title ?>">
                                        <?php for ($b = 0; $b < count($menu->main_menu[$i]->parent_menu, true); $b++):
                                            if (!isset($menu->main_menu[$i]->parent_menu[$b]->parent_submenu)): ?>
                                                <li><a href="<?php echo base_url() .
                                                        $menu->main_menu[$i]->parent_menu[$b]->url ?>"><?php echo
                                                        $menu->main_menu[$i]->parent_menu[$b]->title ?></a></li>
                                            <?php else: ?>
                                                <li class="dropdown dropdown-submenu"><a href="<?php echo base_url() .
                                                        $menu->main_menu[$i]->parent_menu[$b]->url ?>"
                                                                                         class="dropdown-toggle"
                                                                                         data-toggle="dropdown"><?php echo
                                                        $menu->main_menu[$i]->parent_menu[$b]->title ?></a>
                                                    <?php if (isset
                                                    ($menu->main_menu[$i]->parent_menu[$b]->parent_submenu)):
                                      ?>
                                                        <ul class="dropdown-menu">
                                                            <?php foreach
                                                            ($menu->main_menu[$i]->parent_menu[$b]->parent_submenu
                                                             as $par_sub) :

                                                              ?>
                                                                <li><a href="<?php echo
                                                                    $par_sub->url ?>"><?php echo
                                                                        $par_sub->title ?>
                                                                    </a></li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>

                                                </li>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                        <?php } ?>
                    
</ul>
        <div class="btn-sing float-right">
          <a class="btn btn-border" href="../html/page-login.html">Ingresar</a>
        </div>
      </div>
    </div>
<ul class="mobile-menu navbar-nav">
        <?php for ($i = 0; $i < count($menu->main_menu, true); $i++) { ?>
                            <?php if (count($menu->main_menu[$i]->parent_menu, true) == 0): ?>
                                <li class="nav-item"><a
                                            href="<?php echo base_url() . $menu->main_menu[$i]->url ?>" class="nav-link">
                                        <?php echo
                                        $menu->main_menu[$i]->title ?></a></li>
                            <?php else: ?>
                                <li class="dropdown nav-item">
                                    <a href="#"
                                    class="dropdown-toggle nav-link" data-toggle="dropdown">
                                        <span class="nav-link-text"><?php
                                        echo $menu->main_menu[$i]->title ?>   <i class="fa fa-fw fa-caret-down"></i>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu" id="<?php
                                        echo $menu->main_menu[$i]->title ?>">
                                        <?php for ($b = 0; $b < count($menu->main_menu[$i]->parent_menu, true); $b++):
                                            if (!isset($menu->main_menu[$i]->parent_menu[$b]->parent_submenu)): ?>
                                                <li><a href="<?php echo base_url() .
                                                        $menu->main_menu[$i]->parent_menu[$b]->url ?>"><?php echo
                                                        $menu->main_menu[$i]->parent_menu[$b]->title ?></a></li>
                                            <?php else: ?>
                                                <li class="dropdown dropdown-submenu"><a href="<?php echo base_url() .
                                                        $menu->main_menu[$i]->parent_menu[$b]->url ?>"
                                                                                         class="dropdown-toggle"
                                                                                         data-toggle="dropdown"><?php echo
                                                        $menu->main_menu[$i]->parent_menu[$b]->title ?></a>
                                                    <?php if (isset
                                                    ($menu->main_menu[$i]->parent_menu[$b]->parent_submenu)):
                                      ?>
                                                        <ul class="dropdown-menu">
                                                            <?php foreach
                                                            ($menu->main_menu[$i]->parent_menu[$b]->parent_submenu
                                                             as $par_sub) :

                                                              ?>
                                                                <li><a href="<?php echo
                                                                    $par_sub->url ?>"><?php echo
                                                                        $par_sub->title ?>
                                                                    </a></li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>

                                                </li>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                        <?php } ?>
                        <div class="btn-sing float-right">
          <a class="btn btn-border" href="../html/page-login.html">Acuerdos</a>
        </div>
      </div>
</ul>

</nav>




