
  <div id="hero-area" class="hero-area-bg particles_js">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="contents text-center">
            <h2 class="head-title wow fadeInUp">Esto es una web para contratos<br>
             Music</h2>
            <div class="header-button wow fadeInUp" data-wow-delay="0.3s">
              <a href="<?php echo base_url("usuario/login")?>"
                class="btn btn-common">Ingresar</a>
              <a href="<?php echo base_url("usuario/registrar")?>" class="btn btn-common blush">Registrar</a>
            </div>
          </div>
          <div class="img-thumb text-center wow fadeInUp" data-wow-delay="0.6s">
            <img class="img-fluid" src="assets/img/hero-1.png" alt="">
          </div>
        </div>
      </div>
    </div>
    <div id="particles-js"></div>
  </div>
  <!-- Hero Area End -->

