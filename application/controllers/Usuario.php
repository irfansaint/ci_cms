<?php

defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Usuario extends MY_Controller {
    public function __construct()
 {
        parent::__construct();
        $this->form_validation->set_error_delimiters( $this->config->item( 'error_start_delimiter', 'ion_auth' ), $this->config->item( 'error_end_delimiter', 'ion_auth' ) );

        $this->lang->load( 'auth' );
    }

  
    public function registrar()
 {

        $this->data['title'] = $this->lang->line( 'create_user_heading' );

        if ( $this->ion_auth->logged_in() )
        {
            redirect( 'home', 'refresh' );
        }

        $tables = $this->config->item( 'tables', 'ion_auth' );
        $identity_column = 'email';
        //$this->config->item( 'identity', 'ion_auth' );
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules( 'first_name', $this->lang->line( 'create_user_validation_fname_label' ), 'required' );
        $this->form_validation->set_rules( 'last_name', $this->lang->line( 'create_user_validation_lname_label' ), 'required' );
        if ( $identity_column !== 'email' )
 {
            $this->form_validation->set_rules( 'identity', $this->lang->line( 'create_user_validation_identity_label' ), 'required|is_unique['.$tables['users'].'.'.$identity_column.']' );
            $this->form_validation->set_rules( 'email', $this->lang->line( 'create_user_validation_email_label' ), 'required|valid_email' );
        } else {
            $this->form_validation->set_rules( 'email', $this->lang->line( 'create_user_validation_email_label' ), 'required|valid_email|is_unique[' . $tables['users'] . '.email]' );
        }
        $this->form_validation->set_rules( 'phone', $this->lang->line( 'create_user_validation_phone_label' ), 'trim' );
        $this->form_validation->set_rules( 'password', $this->lang->line( 'create_user_validation_password_label' ), 'required|min_length[' . $this->config->item( 'min_password_length', 'ion_auth' ) . ']|max_length[' . $this->config->item( 'max_password_length', 'ion_auth' ) . ']|matches[password_confirm]' );
        $this->form_validation->set_rules( 'password_confirm', $this->lang->line( 'create_user_validation_password_confirm_label' ), 'required' );

        if ( $this->form_validation->run() == true )
 {
            $email    = strtolower( $this->input->post( 'email' ) );
            $identity = ( $identity_column === 'email' ) ? $email : $this->input->post( 'identity' );
            $password = $this->input->post( 'password' );

            $additional_data = array(
                'first_name' => $this->input->post( 'first_name' ),
                'last_name'  => $this->input->post( 'last_name' ),
                'phone'      => $this->input->post( 'phone' ),
                'active'     => 0
            );
        }
        if ( $this->form_validation->run() == true && $this->ion_auth->register( $identity, $password, $email, $additional_data ) )
 {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata( 'message', $this->ion_auth->messages() );
            redirect( 'home', 'refresh' );
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = ( validation_errors() ? validation_errors() : ( $this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata( 'message' ) ) );

            $this->set_message( $this->data['message'], 'warning' );

            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value( 'first_name' ),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value( 'last_name' ),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value( 'identity' ),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value( 'email' ),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value( 'company' ),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value( 'phone' ),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value( 'password' ),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value( 'password_confirm' ),
            );

            $this->view( 'auth/create_user' )->render( $this->data );
        }
    }

// activate the user
public function activate($id, $code=false)
{
    if ($code !== false)
    {
        $activation = $this->ion_auth->activate($id, $code);
    }
    else if ($this->ion_auth->is_admin())
    {
        $activation = $this->ion_auth->activate($id);
    }

        if ( $activation )
 {
            // redirect them to the auth page
            $this->session->set_flashdata( 'message', $this->ion_auth->messages() );
            redirect( 'home', 'refresh' );
        } else {
            // redirect them to the forgot password page
            $this->session->set_flashdata( 'message', $this->ion_auth->errors() );
            redirect( 'usuarios/forgot_password', 'refresh' );
        }
    }
	// log the user in
	public function login()
	{
		$this->data['title'] = $this->lang->line('login_heading');

		//validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() == true)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('admin/welcome', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('admin/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
				'class' =>'form-control' ,
				'placeholder'=> lang('login_identity_label'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
				'class' =>'form-control' ,
				'placeholder'=>'Ingrese contraseña',
			);

			$this->view('auth/login')->render($this->data);
		}
	}
}

/* End of file  Home.php */