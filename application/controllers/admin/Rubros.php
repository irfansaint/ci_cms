<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rubros extends Authenticated_Controller {
	public $data;
	public function __construct()
	{
		parent::__construct();
		$this->mTitle = 'Rubros de Productos ';
		$this->breadcrumbs->push('Rubros','/rubros');
		$this->load->library('form_builder');
		$this->load->library('datatables');
		//$this->load->model('Language_model','Language_model');
		$this->load->model('Rubro_model');
		//$this->load->library('Datatable', array('model' => 'Rubro_model'));
		$this->load->model('Atributos_model','Atributos_model');

	}

	public function showForm() {

		$form = $this->form_builder->create_form('rubros/showForm',TRUE,['id'=>'form-rubros']);
		$this->data['form'] = $form;
		$this->data['combo']=	$this->Rubro_model->dropdown('id','name');
	
		$this->_rules();

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == true) {
			
			$datos['id']=$this->input->post('id',TRUE);
			$datos['id_parent']=$this->input->post('parent',TRUE);
			$datos['name']=$this->input->post('name');
			$datos['description']=$this->input->post('description');

			$this->Rubro_model->insert($datos);

			$this->session->set_flashdata('message', 'Creado con Exito');
			redirect('/rubros');
		}else{
			$this->set_message(validation_errors() ? validation_errors() : '','warning');

			$this->data['campos']=['id'=>set_value('id',null),
			'name'=>set_value('name'),
			'description'=>set_value('description'),
		];
		$this->data['id_parent']=set_value('parent',0);

		$this->breadcrumbs->push('Nuevo','/rubros/');

	}

	$this->view('rubros/form')->render($this->data);
}


public function editForm($p_id) {

	$form = $this->form_builder->create_form(uri_string(),TRUE,['id'=>'form-rubros']);
	$this->data['form'] = $form;
	$this->data['combo']=	$this->Rubro_model->dropdown('id','name');
	$this->breadcrumbs->push('Modificar','/rubros/');

	$this->_rules();

	$this->data['campos']=['id'=>set_value('id'),
	'name'=>set_value('name'),
	'description'=>set_value('description'),
];
$this->data['id_parent']=set_value('parent');;
if ($this->form_validation->run() == true) {

	$datos['id']=$this->input->post('id',TRUE);
	$datos['id_parent']=$this->input->post('parent',TRUE);
	$datos['name']=$this->input->post('name');
	$datos['description']=$this->input->post('description');
	$user = $this->ion_auth->user()->row();

	$this->Rubro_model->update($datos['id'],$datos);

	$this->session->set_flashdata('message', 'Modificado con Exito');
	redirect('/rubros');
}elseif(is_numeric($p_id)){

	$campos = $this->Rubro_model->as_array()->get($p_id);

	if(!is_array($campos)){
		$this->set_message('No hay Rubros para Modificar','warning');
		redirect('/rubros');
	}

	$this->data['id_parent']=$campos['id_parent'];
	$this->data['campos']=['id'=>$campos['id'],
	'name'=>$campos['name'],
	'description'=>$campos['description']];

}


$this->view('rubros/form')->render($this->data);
}

protected function _rules()
{
	$this->form_validation->set_rules('name', 'Titulo', 'trim|required');
	$this->form_validation->set_error_delimiters('<span class="text">', '</span>');
}

public function index() {

		$this->render();
}

    public function datatable()
    {
       /* $this->datatable->setColumnSearchType('$.parent_name', ' none'); // specific column name, colname LIKE %xxx

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(
        );

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($json));*/
		header('Content-Type: application/json');
		echo $this->Rubro_model->json();
		
    }

/**
 * muestras los rubros para cargar
 */
 function showAtributosRubro($id){

	$this->load->helper('security');
			 //$id  =  $this->security->xss_clean($this->input->post('id'));
			$data = $this->Atributos_model->as_array()->with('valores')->get_many_by('id_rubro',$id);
			$form = $this->form_builder->create_form('admin/atributos/addAttrib',TRUE,['id'=>'form-attrib']);
			$salida=null;
		   foreach ($data as $key => $value) {
			 $combo=NULL;
			  if($value['valores']){
				//echo json_encode($value['valores']);
				foreach ($value['valores'] as $v) {
				  $combo[$v->id]=$v->name;
				}
			  }
				$name= underscore($value['attrib_name']);
				$label= $value['attrib_label']?$value['attrib_label']:$value['attrib_name'];
			/*	'text'=>'Texto',
				'date'=>'Fecha',
				'number'=>'Numero',
				'bool'=>'Si/No',
				'combo'=>'Combo',
				'multi'=>'Multiple Seleccion',
				*/
				switch ($value['attrib_value']) {
					case 'combo':
						$salida .=  $form->bs3_dropdown($label,$name,$combo);
					break;
					case 'multi':
					$check=null;
								$salida .='  <div class="form-group">'.form_label($label,$name);
						foreach ($combo as $k => $va) {
							$check.='  <div class="checkbox">';
							$check.= form_checkbox($name.'[]',$k).form_label($va);
							$check.='  </div>';
											}
						$salida .=$check."</div>";
					break;
					case 'text':
						$salida .=  $form->bs3_text($label,$name);
					break;
					case 'bool':
						$salida .='<div class="form-group">'.form_label($label,$name);
						$radio=null;
						foreach ($combo as $k => $va) {
							$radio.='  <label for="char">';
							$radio.= form_radio($name,$k).form_label($va);
							$radio.='  </label>';
											}
						
						$salida .=$radio."</div>";
					break;
					
				}
			

			}
			echo $salida;
}


/**
 * Funcion por ajax para modificar las atributos del producto
 * @return [type] [description]
 */
 function getAttribMod(){

	$this->load->library('security');
			 $id  =  $this->security->xss_clean($this->input->post('id'));
			 $id_producto  =  $this->security->xss_clean($this->input->post('producto'));

			$data = $this->Atributos_model->as_array()->with('valores')->get_many_by('id_rubro',$id);
			$this->load->model('product_attribs_model');

		   $produc_val= $this->product_attribs_model->as_array()->get_many($id_producto);


			foreach ($produc_val as $value) {
			  $select_val[$value['id_attrib_valor']]=$value['id_attrib_valor'];
			}


			$form = $this->form_builder->create_form('admin/atributos/addAttrib',TRUE,['id'=>'form-attrib']);

		   foreach ($data as $key => $value) {
			 $combo=NULL;
			  if($value['valores']){
				//echo json_encode($value['valores']);
				foreach ($value['valores'] as $v) {
				  $combo[$v->id]=$v->name;
				  if(array_key_exists($v->id, $select_val)){ #solo para el combo
					$sel=$v->id;
				  }
				}
			  }
				$name= underscore($value['attrib_name']);

			 if($value['attrib_value']=='combo'){

			  $salida .=  $form->bs3_dropdown($value['attrib_name'],$name,$combo,$sel );
			}else{

			  $salida .='  <div class="form-group">'.form_label($value['attrib_name'],$name);
			  foreach ($combo as $k => $va) {
				$check.='  <div class="checkbox">';
				$check.= form_checkbox($name.'[]',$k,array_key_exists($k, $select_val)).form_label($va);
				$check.='  </div>';
								  }
			  $salida .=$check."</div>";


				}

			}
			echo $salida;
}

}
?>
