<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends Authenticated_Controller {

   
    public $image_file = null;
    
    public function __construct() {
        $this->model_file = 'Opciones_model'; //se carga al Modelo
        parent::__construct();
        
        
    }

    public function index() {
        $this->load->library("form_validation");
        $config['upload_path'] = UPLOAD_IMG;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|ico';
        $this->load->library('upload', $config);
        $data['item'] = new stdClass();
        foreach ($this->Opciones_model->get_all() as $setting) {
            $data['item']->{ $setting->key } = $setting->value;
            $this->form_validation->set_rules('setting[' . $setting->key . ']', 'lang:settings_' . $setting->key, "trim");
        }
        $this->form_validation->set_rules('logo', 'Logo', "callback_logo");
        $this->form_validation->set_rules('favicon', 'Favicon', "callback_favicon");

        if ($this->form_validation->run() == false) {
            $this->view("settings/manage")->render($data);
        } else {
            foreach ($this->input->post('setting') as $key => $value) {
               
                $info['value'] = $value;
                $this->Opciones_model->update($key,$info);
            }



            redirect(site_url('settings'));
        }
    }

    public function logo($var) {

        if ($this->upload->do_upload('logo')) {
            $f = $this->upload->data();
            if ($f['file_name']) {
                $data['value'] = $f['file_name'];
                $this->Opciones_model->update( 'logo',$data);
            }
        }
        return true;
    }

    public function favicon($var) {

        if ($this->upload->do_upload('favicon')) {
            $d = $this->upload->data();
            if ($d['file_name']) {
                              
                $data['value'] =  $d['file_name'];
                $this->Opciones_model->update( 'favicon',$data);
            }
        }
        return true;
    }

}
