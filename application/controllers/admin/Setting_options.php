<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting_options extends Authenticated_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Setting_options_model');
      
        $this->load->model('Settings_model');
    }

    /*public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'setting_options/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'setting_options/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'setting_options/index';
            $config['first_url'] = base_url() . 'setting_options/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Setting_options_model->total_rows($q);
        $setting_options = $this->Setting_options_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'setting_options_data' => $setting_options,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->view('setting_options/setting_options_list')->render( $data);
    }*/

    
    public function read($id) 
    {
        $row = $this->Setting_options_model->with('setting')->get($id);

        var_dump($row);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'id_setting' => $row->id_setting,
		'name' => $row->name,
		'descrip' => $row->descrip,
		'value' => $row->value,
		'autoload' => $row->autoload,
	    );
            $this->view('setting_options/setting_options_read')->render( $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting_options'));
        }
    }

    public function create($_id_setting) 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('setting_options/create_action'),
        'id' => set_value('id'),
        'combo'=> $this->Settings_model->dropdown(),
	    'id_setting' => $_id_setting,//set_value('setting'),
	    'name' => set_value('name'),
	    'descrip' => set_value('descrip'),
	    'value' => set_value('value'),
	    'autoload' => set_value('autoload'),
	);
        $this->view('setting_options/setting_options_form')->render( $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create($this->input->post('id_setting'));
        } else {
            $data = array(
		'id_setting' => $this->input->post('id_setting',TRUE),
		'name' => $this->input->post('name',TRUE),
		'descrip' => $this->input->post('descrip',TRUE),
		'value' => $this->input->post('value',TRUE),
		'autoload' => $this->input->post('autoload',TRUE),
	    );

            $this->Setting_options_model->insert($data);

            $this->session->set_flashdata('message', 'Opcion Create');
     //       redirect(site_url('setting_options'));
                   redirect(site_url('settings'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Setting_options_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('setting_options/update_action'),
		'id' => set_value('id', $row->id),
		'id_setting' => set_value('id_setting', $row->id_setting),
		'name' => set_value('name', $row->name),
		'descrip' => set_value('descrip', $row->descrip),
		'value' => set_value('value', $row->value),
		'autoload' => set_value('autoload', $row->autoload),
	    );
            $this->view('setting_options/setting_options_form')->render( $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting_options'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_setting' => $this->input->post('id_setting',TRUE),
		'name' => $this->input->post('name',TRUE),
		'descrip' => $this->input->post('descrip',TRUE),
		'value' => $this->input->post('value',TRUE),
		'autoload' => $this->input->post('autoload',TRUE),
	    );

            $this->Setting_options_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Opcion Update');
         //   redirect(site_url('setting_options'));
                   redirect(site_url('settings'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Setting_options_model->get_by_id($id);

        if ($row) {
            $this->Setting_options_model->delete($id);
            $this->session->set_flashdata('message', 'Opcion Delete');
            redirect(site_url('setting_options'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting_options'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_setting', 'Setting Padre', 'trim|required');
	$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
	$this->form_validation->set_rules('descrip', 'descrip', 'trim|required');
	$this->form_validation->set_rules('value', 'value', 'trim|required');
	$this->form_validation->set_rules('autoload', 'autoload', 'trim|required');

	
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
