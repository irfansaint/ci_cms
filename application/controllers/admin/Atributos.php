<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Atributos extends Authenticated_Controller
{

    public function __construct()
    {
        parent::__construct();

     
        $this->load->model('Atributos_model', 'Atributos_model');
        $this->load->model('Valores_model', 'Valores_model');
    }

    public function agregar($id_rubro)
    {

        $form                        = $this->form_builder->create_form('atributos/addAttrib', true, ['id' => 'form-attrib']);
        
        $this->mTitle                = "Atributos de Rubros ";
        $this->mViewData['id_rubro'] = $id_rubro;
        
        $this->mViewData['form']     = $form;
        $this->mViewData['datos']    = $this->Atributos_model->as_array()->with('valores')->get_many_by('id_rubro', $id_rubro);
        $this->mViewData['formitem'] = $this->form_builder->create_form('atributos/addItemAttrib', true, ['id' => 'form-item-attrib']);

        $this->add_style('vendor/bootstrap3-editable/bootstrap3-editable/css/bootstrap-editable.css', true);
        $this->add_script('vendor/bootstrap3-editable/bootstrap3-editable/js/bootstrap-editable.min.js', true, 'head');

        $this->view('atributos/form')->render($this->mViewData);
    }
/**
 * agega un atributo
 * @param string $value [description]
 */
    public function addAttrib()
    {

        $user = $this->ion_auth->user()->row();
        $dato = ['id_rubro' => $this->input->post('id_rubro', true),
            'attrib_label'       => $this->input->post('atributo_label', true),
            'attrib_name'       => $this->input->post('atributo_name', true),
            'attrib_value'      => $this->input->post('atributo_presentacion', true),
        ];

        $id = $this->Atributos_model->insert($dato);

        if ($id) {

            $json = $this->Atributos_model->get($id);
            $this->render_json($json); 
        } else {
            echo json_encode('ko');
        }
    }

    public function modNameAttrib()
    {
        $user = $this->ion_auth->user()->row();
        $dato = [
            'attrib_name' => $this->input->post('value', true),

        ];
        $pk = $this->input->post('pk', true);
        $id = $this->Atributos_model->update($pk, $dato);
        if ($id) {
            echo $this->Atributos_model->as_json()->get($id);
        } else {
            echo json_encode('ko');
        }
    }

/**
 * elimina atributo
 * @return [type] [description]
 */
    public function delAttrib()
    {
        $id    = $this->input->post('data', true);
        $datos = $this->Atributos_model->with('valores')->get($id);

        if ($datos->valores) {
            echo json_encode('ko');
        } else {
            echo $this->Atributos_model->delete($id);
        }

    }

    public function getValoresAttrib()
    {
        $id = $this->input->post('idattrib', true);

        if ($id) {
            echo $this->Atributos_model->as_json()->with('valores')->get($id);
        } else {
            echo json_encode('ko');
        }
    }
    /**
     * agrega un item a los Atributos del Rubros
     */
    public function addItemAttrib()
    {
        $item = $this->input->post('atributo_valor', true);
        $dato = ['id_rubros_attribs' => $this->input->post('id_atrrib', true),
            'name'                       => ucwords($item),
            'value'                      => underscore($item),
        ];

        $id = $this->Valores_model->insert($dato);

        if ($id) {
            echo $this->Valores_model->as_json()->get($id);
        } else {
            echo json_encode('ko');
        }
    }
    public function delItemAttrib()
    {

        $id    = $this->input->post('data', true);
        $datos = $this->Valores_model->get($id);

        /*if($datos->valores)
        echo json_encode('ko');  //CONTROLAR SI HAY RELACIONES
        else*/
        echo $this->Valores_model->delete($id);
    }
    /**
     * Modifica el nombre de los Valores del atributo
     * @return {[type] [description]
     */
    public function modNameItemAttrib()
    {
        $item = $this->input->post('value', true);
        $dato = [
            'name'  => ucwords($item),
            'value' => underscore($item),
        ];
        $pk = $this->input->post('pk', true);

        $id = $this->Valores_model->update($pk, $dato);
        if ($id) {
            echo $this->Valores_model->as_json()->get($id);
        } else {
            echo json_encode('ko');
        }
    }

}
