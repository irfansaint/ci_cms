<?php
//require APPPATH."core/MY_Model.php";

class Atributos_model extends MY_Model{

  
  
  public $has_many = array('valores' =>
                                  array( 'model' =>'Valores_model' ,
                                  'primary_key' => 'id_rubros_attribs')
                      );

                      function __construct(){
                        parent::__construct();
                        $this->_table = 'rubros_attribs';
                        $this->primary_key = 'id';
                        $this->order = 'DESC';
                        $this->soft_delete = TRUE;
                        $this->soft_delete_key_full = 'rubros_attribs.deleted';
                        $this->before_create = array( 'created_at', 'created_by' );
                        $this->before_update = array( 'updated_at','updated_by');
                        $this->before_delete = array( 'deleted_at','deleted_by' );
                        
                       
                        $this->user_id_getter='user_id_getter_for_models';
                      }
                    

  }
 ?>
