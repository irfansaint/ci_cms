<?php
//require APPPATH."core/MY_Model.php";

class Categoria_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'categorys';
        $this->order = 'DESC';
        $this->soft_delete = true;
        $this->soft_delete_key_full = 'categorys.deleted'; //para realizar filtro soft delete
        $this->before_create = array('created_at', 'created_by');
        $this->before_update = array('updated_at', 'updated_by');
        $this->before_delete = array('deleted_at', 'deleted_by');

        $this->user_id_getter = 'user_id_getter_for_models';
    }

    public function dropdown()
    {
        $result = $this->db->select('id,name')
            ->where(' id_parent= 0')
            ->order_by('name', 'asc')
            ->get($this->_table)

            ->result();
        if (count($result) > 0) {

            foreach ($result as $row) {
                $options[$row->id] = $row->name;
            }
            return $options;
        }
        return array();
    }

    // datatables
    public function json()
    {
        $this->datatables->select('r.id,r.name,r.activo,c.name as parent_name');
        $this->datatables->from($this->_table . ' r');
        $this->datatables->join('categorys c', 'r.id_parent = c.id', 'left');
        $this->datatables->like('r.deleted', '0');

        return $this->datatables->generate();
    }

    public function dropdownTree($_all = true)
    {

        if ($_all) {
            $padres = $this->dropdown();
            $result = $this->db->select('id,name,id_parent')
                ->where('activo = 1 and id_parent!= 0')
                ->get($this->_table)
                ->result();
            foreach ($result as $row) {
                $options[$row->id] = $row->name;
                $tree[$padres[$row->id_parent]][$row->id] = $row->name;

            }

        } else {

            $result = $this->db->select('id,name,id_parent')
                ->where('activo = 1 ')
                ->get($this->_table)
                ->result();
            foreach ($result as $row) {
                // $options[$row->id] = $row->name;
                $tree[$row->id] = $row->name;

            }
        }
        return $tree;

    }
    /**
    * select2
    *
    * @param [type] $q
    * @return void
    */

    function get_sync( $q = NULL )
 {
        $this->db->select( 'id,name as text' );
        $this->db->order_by( 'name', 'asc' );
        if ( $q )
        $this->db->where( $q );

        $this->db->limit( 10 );

        return $this->db->get( $this->_table )->result();
    }
/**
 * trae desde el id POST
 *
 * @param [type] $post_id
 * @return void
 */
    public function get_by_post_id($post_id)
	{
		$this->db->select($this->_table.'.id,name');
		$this->db->join('post_categorys', $this->_table.'.id = post_categorys.category_id', 'RIGHT');
		$this->db->where('post_categorys.post_id', $post_id);
		$result = $this->db->get($this->_table)->result();
        $categotias=[];
        foreach ($result as $row) {
         
            $categotias[$row->id] = $row->name;

        }
        return $categotias;
    }
    
    public function add_relacion($post_id,$cat_id){
        $this->db->insert('post_categorys',['post_id'=>$post_id,'category_id'=>$cat_id]);
    }
    public function del_relacion($post_id){
        $this->db->delete('post_categorys',['post_id'=>$post_id]);
    }


}
