<?php
  //require APPPATH."core/MY_Model.php";

class Rubro_model extends MY_Model {

  public $has_many = array('atributos' =>array
    ('model' =>'Atributos_model' ,
    'primary_key' => 'id_rubro')
);


  function __construct(){
    parent::__construct();
    $this->_table = 'rubros';
    
    $this->order = 'DESC';
    $this->soft_delete = TRUE;
    $this->before_create = array( 'created_at', 'created_by' );
    $this->before_update = array( 'updated_at','updated_by');
    $this->before_delete = array( 'deleted_at','deleted_by' );
    $this->soft_delete = TRUE;
   
    $this->user_id_getter='user_id_getter_for_models';
  }

    

    // datatables
    function json() {
      $this->datatables->select('r.id,r.name,r.id_parent,rf.name as parent_name');
      $this->datatables->from($this->_table.' r');
      $this->datatables->like('r.deleted', '0');
      //add this line for join
      $this->datatables->join('rubros rf', 'rf.id = r.id_parent','left');
      //$this->datatables->add_column('action', anchor(site_url('proveedor/read/$1'),'Leer',['class'=>'btn btn-default'])." | ".anchor(site_url('proveedor/update/$1'),'Editar',['class'=>'btn btn-default'])." | ".anchor(site_url('proveedor/delete/$1'),'Borrar','onclick="javasciprt: return confirm(\'Estas seguro ?\')"'), 'id');

      return $this->datatables->generate();
  }

 public function dropdownTree()
 {
  $padres=$this->dropdown();
  $result =  $this->db->select('id,name,id_parent')
  ->where('activo = 1 and id_parent!= 0')
  ->get($this->_table)
  ->result();

  foreach ($result as $row)
  {
    $options[$row->id] = $row->name;
    $tree[$padres[$row->id_parent]][$row->id]=  $row->name;

  }
  return $tree;
}
}
?>
