<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends MY_Model
{

    public $_table = 'settings';
    public $primary_key = 'id';
    public $order = 'DESC';
    public $has_many =array(
        'options' =>
       array( 'model' =>'Settings_options_model' ,
        'primary_key' => 'id_setting')
      );
    
   
   
    public function dropdown()
    {
      $result =  $this->db->select('id,name')
      ->get($this->_table)
      ->result();
          if(count($result)>0){
  
              foreach ($result as $row)
              {
                $options[$row->id] = $row->name;
              }
              return $options;
          }
      return array();
    }
  
    

    // get all
    function get_all()
    {
        $this->db->order_by($this->primary_key, $this->order);
        return $this->db->get($this->_table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->get($this->_table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('name', $q);
	$this->db->from($this->_table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->primary_key, $this->order);
        $this->db->like('id', $q);
    $this->db->or_like('name', $q);
    	$this->db->limit($limit, $start);
        return $this->db->get($this->_table)->result();
    }

  

}

/* End of file Settings_model.php */
/* Location: ./application/models/Settings_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-28 04:07:57 */
/* http://harviacode.com */