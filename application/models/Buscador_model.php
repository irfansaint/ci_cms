<?php
//require APPPATH."core/MY_Model.php";

class buscador_model extends MY_Model {

  public $_table = 'pacientes';
  public $order = 'DESC';
  
  // datatables
  function json() {
    $this->datatables->select('p.id,p.apellido,p.nombre,p.fecha_nac,p.domicilio,p.telefono');
    $this->datatables->from('pacientes p');
    $this->datatables->like('deleted', '0');
    //add this line for join
    //$this->datatables->join('producto_precio pp', 'p.id = pp.id_producto and pp.id_precio=2');
    $this->datatables->add_column('agregar', anchor(site_url('pacientes/read/$1'),'<i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Ver datos del Paciente"></i>',array('class'=>'btn btn-sm btn-default'))." | ".anchor(site_url('fichas/visitas/$1'),'<i class="fa fa-file-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agregar Consulta"></i>',array('class'=>'btn btn-sm btn-default')), 'id') ;
    
    
    return $this->datatables->generate();
}

  }
 ?>
