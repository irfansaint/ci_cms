<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Articulos_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();

         $this->_table = 'articulos';
     $this->primary_key = 'id';
     $this->order = 'DESC';
     $this->soft_delete = TRUE;
     $this->soft_delete_key_full='articulos.deleted'; //para realizar filtro soft delete
     $this->before_create = array( 'created_at', 'created_by' );
     $this->before_update = array( 'updated_at','updated_by');
     $this->before_delete = array( 'deleted_at','deleted_by' );
     $this->where = array('deleted' => '0');
   
     $this->user_id_getter='user_id_getter_for_models';
    }

    // datatables
    function json() {
        $this->datatables->select('id,titulo,bajada,contenido,activo,created_at,updated_at,deleted_at,deleted,created_by,updated_by,deleted_by');
        $this->datatables->from( $this->_table);
  
        //add this line for join
        //$this->datatables->join('table2', 'articulos.field = table2.field');
        $this->datatables->add_column('action', 
                                        anchor(site_url('admin/articulos/read/$1'),'Leer',['class'=>'btn btn-info btn-circle btn-sm','title'=>'Ver la imformacion'])." 
                                        ".anchor(back_url('articulos/update/$1'),'<i class="fas fa-pencil-alt" ></i>',['class'=>'btn btn-warning btn-circle btn-sm','title'=>'Editar'])." 
                                        ".anchor(back_url('articulos/delete/$1'),'<i class="fas fa-trash"></i>','class="btn btn-danger btn-circle btn-sm" onclick="javasciprt: return confirm(\'Estas seguro ?\')" title="Eliminar"'), 'id');
        return $this->datatables->generate();
    }
   


}

/* End of file Articulos_model.php */
/* Location: ./application/models/Articulos_model.php */
