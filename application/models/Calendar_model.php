<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar_model extends MY_Model {


	

    function __construct()
    {
        parent::__construct();

         $this->_table = 'events';
     $this->primary_key = 'id';
     $this->order = 'DESC';
	 $this->soft_delete = TRUE;
	 $this->soft_delete_key_full='events.deleted'; //para realizar filtro soft delete
     $this->before_create = array( 'created_at', 'created_by' );
     $this->before_update = array( 'updated_at','updated_by');
     $this->before_delete = array( 'deleted_at','deleted_by' );
     
   
     $this->user_id_getter='user_id_getter_for_models';
    }

   

/*Read the data from DB */
	Public function getEvents()
	{
		
	$sql = "SELECT * FROM events 
	left join pacientes p on p.id=id_paciente
	WHERE events.deleted=0 AND events.start BETWEEN ? AND ? 
	
	 ORDER BY events.start ASC";
	return $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();

	}

/*Create new events */

	Public function addEvent()
	{

	$sql = "INSERT INTO events (id_paciente,title,events.start,events.end,description, color) VALUES (?,?,?,?,?,?)";
	$this->db->query($sql, array($_POST['id_paciente'],$_POST['title'], $_POST['start'],$_POST['end'], $_POST['description'], $_POST['color']));
		return ($this->db->affected_rows()!=1)?false:true;
	}

	/*Update  event */

	Public function updateEvent()
	{

	$sql = "UPDATE events SET id_paciente = ?, title = ?, description = ?, color = ? WHERE id = ?";
	$this->db->query($sql, array($_POST['id_paciente'],$_POST['title'],$_POST['description'], $_POST['color'], $_POST['id']));
		return ($this->db->affected_rows()!=1)?false:true;
	}


	/*Delete event */

	Public function deleteEvent()
	{

	/*$sql = "DELETE FROM events WHERE id = ?";
	$this->db->query($sql, array($_GET['id']));*/
	$this->delete($_GET['id']);
		return ($this->db->affected_rows()!=1)?false:true;
	}

	/*Update  event */

	Public function dragUpdateEvent()
	{
			//$date=date('Y-m-d h:i:s',strtotime($_POST['date']));

			$sql = "UPDATE events SET  events.start = ? ,events.end = ?  WHERE id = ?";
			$this->db->query($sql, array($_POST['start'],$_POST['end'], $_POST['id']));
		return ($this->db->affected_rows()!=1)?false:true;


	}






}